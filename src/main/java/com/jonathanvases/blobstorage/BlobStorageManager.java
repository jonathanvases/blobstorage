package com.jonathanvases.blobstorage;

import com.azure.storage.blob.*;
import com.azure.storage.blob.models.UserDelegationKey;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.*;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;

public class BlobStorageManager {


    public String uploadDirect(MultipartFile file) throws Exception {
        String uri = "";
        String connectStr = System.getenv("AZURE_STORAGE_CONNECTION_STRING");

        BlobServiceClient blobServiceClient = new BlobServiceClientBuilder().connectionString(connectStr).buildClient();

        BlobContainerClient containerClient = blobServiceClient.getBlobContainerClient("subida");


        BlobClient blobClient = containerClient.getBlobClient(file.getOriginalFilename());

        uri = blobClient.getBlobUrl();

        blobClient.upload(file.getInputStream(), file.getSize());
        return uri;
    }

    public Mono<String> uploadDirect( Flux<FilePart> fileP, FilePart name) {
        String connectStr = System.getenv("AZURE_STORAGE_CONNECTION_STRING");
        BlobServiceAsyncClient blobServiceClient = new BlobServiceClientBuilder().connectionString(connectStr).buildAsyncClient();
        BlobContainerAsyncClient containerClient = blobServiceClient.getBlobContainerAsyncClient("subidabloqueada");
        BlobAsyncClient blobClient = containerClient.getBlobAsyncClient(name.filename());

        fileP.subscribe(uno -> uno.content().subscribe(dos -> {
            try {
                byte[] bytes = new byte[dos.readableByteCount()];
                dos.read(bytes);
                blobClient.upload(Flux.just(ByteBuffer.wrap(bytes)), null).subscribe();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }));


        return Mono.just(blobClient.getBlobUrl());

    }


    public Mono<String> generateUriWithToken(String nombreArchivo) throws Exception {

        String connectStr = System.getenv("AZURE_STORAGE_CONNECTION_STRING");

        CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(connectStr);
        CloudBlobClient cloudBlobClient = cloudStorageAccount.createCloudBlobClient();
        CloudBlobContainer container = cloudBlobClient.getContainerReference("subidabloqueada");
        CloudBlockBlob blob = container.getBlockBlobReference(nombreArchivo);

        SharedAccessBlobPolicy sasConstraints = new SharedAccessBlobPolicy();
        sasConstraints.setSharedAccessExpiryTime( new Date(new Date().getTime() + 99999999));
        sasConstraints.setPermissions(EnumSet.of(SharedAccessBlobPermissions.WRITE, SharedAccessBlobPermissions.CREATE,SharedAccessBlobPermissions.READ));

        return Mono.just(blob.getUri().toString() + "?" + blob.generateSharedAccessSignature(sasConstraints, null)) ;
    }


    public String uploadFileWithURIToken(MultipartFile file, String URI) throws Exception {

        CloudBlockBlob cloudBlockBlob = new CloudBlockBlob(new URI(URI));


        cloudBlockBlob.upload(file.getInputStream(), file.getSize());


        return cloudBlockBlob.getUri().toString();
    }









}
