package com.jonathanvases.blobstorage;


import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;


import java.io.*;
import java.nio.charset.StandardCharsets;

import java.util.Map;


@Service
public class AlertServiceImpl implements AlertService {

    BlobStorageManager blobStorageManager = new BlobStorageManager();

    @Override
    public Flux<String> sendErrorReport(String desc, Flux<FilePart> filePart, Map<String, String> headers) {
        filePart.subscribe(file -> {
            blobStorageManager.uploadDirect( filePart, file).subscribe();
        });
        return Flux.empty();
    }
}


