package com.jonathanvases.blobstorage;

import org.springframework.http.codec.multipart.FilePart;
import reactor.core.publisher.Flux;

import java.util.Map;


public interface AlertService {
	
	Flux<String> sendErrorReport( String desc, Flux<FilePart> file, Map<String, String> headers);

}
