package com.jonathanvases.blobstorage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController()
@RequestMapping("blobstorage")
public class BlobStorageController {

    BlobStorageManager blobStorageManager=new BlobStorageManager();


    @PostMapping(value = "/uploadwithtoken")
    public String uploadWithToken(@RequestParam("file") MultipartFile files,@RequestParam("uri")String URI)throws Exception{
        ArrayList<String> uris=new ArrayList<>();

        return blobStorageManager.uploadFileWithURIToken(files,URI);
    }



   @Autowired
    AlertService alertService;

    @PostMapping(value = "/generateToken")
    public Mono<String> generateToken(@RequestParam("nombreArchivo") String nombreArchivo)throws Exception{
        ArrayList<String> uris=new ArrayList<>();

        return blobStorageManager.generateUriWithToken(nombreArchivo);
    }

    @PostMapping(value = "/direct",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_STREAM_JSON_VALUE})
    public Flux<String> submitErrorReport(@RequestPart("file") Flux<FilePart> file,
                                          @RequestPart("description") String description,
                                          @RequestHeader Map<String, String> headers) {

        return alertService.sendErrorReport( description,file, headers);
    }




}
